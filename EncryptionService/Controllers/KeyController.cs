﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EncryptionService.Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EncryptionService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KeyController : ControllerBase
    {
        IKeyProvider _keyProvider;
        public KeyController(IKeyProvider keyProvider)
        {
            this._keyProvider = keyProvider;
        }
        public IActionResult Put()
        {
            _keyProvider.GenerateKey();
            return StatusCode(202);
        }
    }
}