using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EncryptionService.Business;
using EncryptionService.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace EncryptionService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EncryptController : ControllerBase
    {
        private readonly IEncryptionProvider _encryptionProvider;
        private readonly IKeyProvider _keyProvider;
        private IServiceCollection _serviceCollection;
        public EncryptController(IKeyProvider keyProvider, IEncryptionProvider encryptionProvider,IServiceCollection serviceCollection)
        {
            this._keyProvider = keyProvider;
            this._encryptionProvider = encryptionProvider;
            this._serviceCollection = serviceCollection;
        }

        [HttpPost]
        public JsonResult Post([FromBody] EncryptionModel encryptionModel)
        {
            var key = _keyProvider.GetActiveKey();

            var result = _encryptionProvider.EncrytptSecret(encryptionModel.data, key).Result;

            return new JsonResult(new EncryptionModel { data = result });
        }

    }

}