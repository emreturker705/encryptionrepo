﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EncryptionService.Business;
using EncryptionService.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace EncryptionService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DecryptController : ControllerBase
    {
        private readonly IEncryptionProvider _encryptionProvider;
        private readonly IKeyProvider _keyProvider;
        private IServiceCollection _serviceCollection;
        public DecryptController(IKeyProvider keyProvider, IEncryptionProvider encryptionProvider, IServiceCollection serviceCollection)
        {
            this._keyProvider = keyProvider;
            this._encryptionProvider = encryptionProvider;
            this._serviceCollection = serviceCollection;
        }

        [HttpPost]
        public JsonResult Post([FromBody] DecryptionModel decryptionModel)
        {
            var key = _keyProvider.GetActiveKey();
            var decryptedTest = _encryptionProvider.DecryptSecret(key,decryptionModel.secret);

            return new JsonResult(new DecryptionModel { secret = decryptedTest.Result.ToString() });
        }
    }
}