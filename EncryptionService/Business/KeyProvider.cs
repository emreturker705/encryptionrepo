using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
namespace EncryptionService.Business
{
     public class KeyProvider : IKeyProvider
    {
        IServiceCollection _serviceCollection;
      
        public KeyProvider(IServiceCollection serviceCollection)
        {
            _serviceCollection = serviceCollection;
        }

        public string GetActiveKey()
        {
            _serviceCollection.AddDataProtection()
                // point at a specific folder and use DPAPI to encrypt keys
                .PersistKeysToFileSystem(new DirectoryInfo(@"c:\temp-keys"))
                .ProtectKeysWithDpapi();

            var services = _serviceCollection.BuildServiceProvider();

            // one key in the key ring
            services.GetDataProtector("Sample.KeyManager.v1").Protect("payload");


            // get a reference to the key manager
            var keyManager = services.GetService<IKeyManager>();

            var key = keyManager.GetAllKeys().OrderByDescending(x => x.CreationDate).FirstOrDefault()?.KeyId.ToString();

            if (key == null)
            {
                key = this.GetActiveKey().ToString();
            }

            return key;
        }

        public async Task<Guid?> GetKeyWithId(Guid id)
        {
            _serviceCollection.AddDataProtection()
                            // point at a specific folder and use DPAPI to encrypt keys
                            .PersistKeysToFileSystem(new DirectoryInfo(@"c:\temp-keys"))
                            .ProtectKeysWithDpapi();

            var services = _serviceCollection.BuildServiceProvider();

            // one key in the key ring
            services.GetDataProtector("Sample.KeyManager.v1").Protect("payload");

            // get a reference to the key manager
            var keyManager = services.GetService<IKeyManager>();

            return await Task.FromResult<Guid?>(keyManager.GetAllKeys().FirstOrDefault(x => x.KeyId == id)?.KeyId ?? null);

        }

        Task<string> IKeyProvider.GenerateKey()
        {
            try
            {
                _serviceCollection.AddDataProtection()
                // point at a specific folder and use DPAPI to encrypt keys
                .PersistKeysToFileSystem(new DirectoryInfo(@"c:\temp-keys"))
                .ProtectKeysWithDpapi();

                var services = _serviceCollection.BuildServiceProvider();

                // one key in the key ring
                services.GetDataProtector("Sample.KeyManager.v1").Protect("payload");


                // get a reference to the key manager
                var keyManager = services.GetService<IKeyManager>();

                // add a new key to the key ring with immediate activation and a 1-month expiration
                var result = keyManager.CreateNewKey(
                     activationDate: DateTimeOffset.Now,
                     expirationDate: DateTimeOffset.Now.AddDays(1));

                return Task.FromResult<string>(result.KeyId.ToString());
            }
            catch (Exception ex)
            {
                return  Task.FromResult<string>("Something went wrong when trying GenerateKey. Reason :" + ex.Message);
            }
            
        }
    
    }
}