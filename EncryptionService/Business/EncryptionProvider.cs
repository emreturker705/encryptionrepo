using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Amazon.S3.Encryption;
using Amazon.Runtime;
using Amazon;
using Amazon.S3.Model;
using Amazon.S3;
using System.Text;
using System.Buffers;

namespace EncryptionService.Business
{
    public class EncryptionProvider : IEncryptionProvider
    {
        IServiceCollection _serviceCollection;

        const string BUCKETNAME = "emreturkertestbucket";

        EncryptionMaterials kmsEncryptionMaterials;

        AWSCredentials credentials;
        public EncryptionProvider(IServiceCollection serviceCollection)
        {
            _serviceCollection = serviceCollection;

            kmsEncryptionMaterials = new EncryptionMaterials("arn:aws:kms:us-east-2:165260327138:key/416f2589-566d-4158-82df-28ec34507590");
            //TODO: The keys should move the local profile
            credentials = new BasicAWSCredentials("AKIASM6SIPDREOGIE5C5", "+89DD2JLeHLNDt49nHJ34EhaJFr/TBhcK0AKZRgy");

        }

        public async Task<string> EncrytptSecret(string secret, string key)
        {
            try
            {
                using (var s3Client = new AmazonS3EncryptionClient(credentials, RegionEndpoint.USEast2, kmsEncryptionMaterials))
                {
                    var putRequest = new PutObjectRequest
                    {
                        BucketName = BUCKETNAME,
                        Key = key,
                        ContentBody = secret,
                        ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256
                    };

                    CopyObjectRequest request = new CopyObjectRequest
                    {
                        SourceBucket = BUCKETNAME,
                        SourceKey = key,
                        DestinationBucket = BUCKETNAME,
                        DestinationKey = key
                    };

                   var result = await s3Client.PutObjectAsync(putRequest);

                    CopyObjectResponse response = await s3Client.CopyObjectAsync(request);

                    return await Task.FromResult<string>(result.ETag);
                }
            }
            catch (System.Exception ex)
            {
                return await Task.FromResult<string>("Internal server error on AmazonS3EncryptionClient Reason :" + ex.Message);
            }


        }
        public async Task<string> DecryptSecret(string key,string secret)
        {
            try
            {
                using (var s3Client = new AmazonS3EncryptionClient(credentials, RegionEndpoint.USEast2, kmsEncryptionMaterials))
                {
                    var getRequest = new GetObjectRequest
                    {
                        BucketName = BUCKETNAME,
                        Key = key,
                        EtagToMatch = secret
                    };

                    string responseBody;
                    using (var response = await s3Client.GetObjectAsync(getRequest))
                    {
                        using (var responseStream = response.ResponseStream)
                        {

                            using (var reader = new StreamReader(responseStream))
                            {
                                //if response stream null set ETag
                                responseBody = reader.ReadToEnd() ?? response.ETag;
                            }
                        }
                    }
                    return await Task.FromResult<string>(responseBody.ToString());

                }
            }
            catch (System.Exception ex)
            {
                return await Task.FromResult<string>("Internal server error on AmazonS3EncryptionClient. Reason :" + ex.Message);
            }
        }



        private async Task<List<string>> GetListOfStringsFromStream(Stream requestBody)
        {
            // Build up the request body in a string builder.
            StringBuilder builder = new StringBuilder();

            // Rent a shared buffer to write the request body into.
            byte[] buffer = ArrayPool<byte>.Shared.Rent(4096);

            while (true)
            {
                var bytesRemaining = await requestBody.ReadAsync(buffer, 0, buffer.Length);
                if (bytesRemaining == 0)
                {
                    break;
                }

                // Append the encoded string into the string builder.
                var encodedString = Encoding.UTF8.GetString(buffer, 0, bytesRemaining);
                builder.Append(encodedString);
            }

            ArrayPool<byte>.Shared.Return(buffer);

            var entireRequestBody = builder.ToString();

            // Split on \n in the string.
            return new List<string>(entireRequestBody.Split("\n"));
        }

    }
}