using System;
using System.Threading.Tasks;

namespace EncryptionService.Business
{
    public interface IKeyProvider
    {
        Task<string> GenerateKey();
        Task<Guid?> GetKeyWithId(Guid id);
        string GetActiveKey();
    }
}