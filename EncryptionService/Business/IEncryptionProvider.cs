using System;
using System.Threading.Tasks;

namespace EncryptionService.Business
{
    public interface IEncryptionProvider
    {
         Task<string> EncrytptSecret(string secret,string key);
         Task<string> DecryptSecret(string key,string secret);
    }
}